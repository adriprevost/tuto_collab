# tuto_collab

## préparer le terrain

### cloner un répertoire distant

Dans son home, créer un répertoire git pour y stocker tout les projets git.

```
cd
mkdir git
cd git/
git clone "https://gitlab.com/aleea/test_rep"
```
### Vérifier les branches disponibles

Cette commande va lister toutes les branches locales puis toutes les branches distantes du repertoire. La branche active est précédée d'un *.

```
cd test_rep
git branch -a
```

### Au besoin changer de branche

git checkout "branche"

La nouvelle branch active est "branche"

## Après avoir travaillé

### Lister le status du répertoire

```
git status
```

### Ajouter les fichiers choisis

```
git add * # pour ajouter tout les fichiers
git add monfichier.txt  # pour ajouter seulement le fichier monfichier.txt
```

### Commit

```
git commit -m "Ceci est un super commit"
```

### On pousse vers le répertoire distant dans la branche branche!

```
git push
```

### Le contraire

```
git pull
```

récupère depuis le distant et le met dans le local

## Fusionner deux branches

Pour fusionner la branche "branche" à la branche master

Il faut que la branche "branche" soit pousser sur le repertoire distant. Puis suivre la doc pour faire un merge request.

# https://stackoverflow.com/questions/34119866/setting-up-and-using-meld-as-your-git-difftool-and-mergetool
# http://michaelwales.com/articles/make-gitconfig-work-for-you/

## Se connecter en SSH

*se placer dans le répertoire git

*générer une clé publique et privée

*copier sa clé publique et l'insérer dans gitlab
